Shopping cart.

Source main folder consists of three classes: Cart, Product and ProductInCart.

Cart symbolizes customer's shopping cart. It has one field - list of products grouped in containers. You can add or remove products from cart, as well as count total price and total amount of products in cart.

Product symbolizes product, which can be bought by customer. It has three fields - name, description and price. When you create new product, you must know values of all fields.

ProductInCart symbolizes container, which groups products of the same type. For example: when customer wants to buy two eggs, there will be two objects of type Product, but only one container. It has two fields - product and quantity. In addition to getters and setters for all fields, it has also two methods, by which you can increment and decrement (by one) quantity of products in container.

Source test folder consists of two classes: CartTest and ProductInCartTest.

CartTest tests methods in Cart class and ProductsInCartTest tests methods in ProductInCart class.

There is no method "main", which runs the application and displays shopping cart in console due to generality of code, which was a priority. I didn't want to impose the way how it should be displayed.