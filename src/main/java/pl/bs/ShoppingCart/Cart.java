package pl.bs.ShoppingCart;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/** This class has one field, which is a list of ProductInCart (list of containers). 
 * It has methods: adding new product to cart, removing existing product from cart, 
 * counting total price of products in cart and counting total quantity of products in cart.
 * @author malgorzata.syska@o2.pl */
public class Cart {
	
	public List<ProductInCart> listOfProducts = new ArrayList<>();
	
	/** It adds new product to cart. Firstly, it checks if in cart there is a container (ProductInCart), 
	 * where it can place new product. Then, if there is none, it creates one and add new product (in container) to it. 
	 * If there is a suitable container (ProductInCart) - it increase quantity of this kind of product in container.
	 * @param product which you want to add	 */
	public void addToCart (Product product) {
		ProductInCart foundProduct = findProductInCart(product);
		if (foundProduct == null) {
			ProductInCart basket = new ProductInCart(product,1);
			listOfProducts.add(basket);
		}
		else {
			foundProduct.increaseQuantityByOne();
		}
	}
	
	/** It finds a container of products (given in parameter) in cart. If it finds it - it returns this container.
	 *  If not - it returns null.
	 * @param product which you want to find in cart
	 * @return container of products or null	 */
	private ProductInCart findProductInCart(Product product) {
		for (ProductInCart p : listOfProducts) {
			if (p.getProduct().equals(product)) {
				return p;
			}
		}
		return null;
	}
	
	/** It removes product from cart. Firstly, it checks if in cart there is a product given in parameter. 
	 * Then, it removes product from cart. If it was the last product in its container - it deletes container as well. 
	 * @param product which you want to remove from cart	 */
	public void removeFromCart (Product product) {
		ProductInCart foundProduct = findProductInCart(product);
		if (foundProduct != null) {
			foundProduct.decreaseQuantityByOne();
			if (foundProduct.getQuantity() == 0) {
				listOfProducts.remove(foundProduct);
			}
		}
	}
	
	/** It counts total amount of products in cart.
	 * @return total amount of products in cart.	 */
	public int countProducts () {
		int i = 0;
		for (ProductInCart p : listOfProducts) {
			i = i + p.getQuantity();
		}
		return i;
	}
	
	/** It counts total price of products in cart.
	 * @return total price of products in cart scaled to two places after comma	and round half up */
	public BigDecimal countPrice () {
		BigDecimal totalPrice = new BigDecimal(0);
		for (ProductInCart p : listOfProducts) {
			totalPrice = totalPrice.add(p.getProduct().getPrice().multiply(new BigDecimal(p.getQuantity())));
		}
		return totalPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

}
