package pl.bs.ShoppingCart;

import java.math.BigDecimal;

/** This class contains three fields: (String) name, (String) description and (BigDecimal) price. It has one constructor, 
 * which demands values for all three fields. 
 * @author malgorzata.syska@o2.pl */
public class Product {
	
	private String name;
	private String description;
	private BigDecimal price;
	
	Product(String name, String description, BigDecimal price) {
		this.name = name;
		this.description = description;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
