package pl.bs.ShoppingCart;

/** This class contains two fields: (Product) product and (int) quantity. This is a kind of container, 
 * which group the same kind of products into one container. 
 * It has one constructor, which demands values for all fields. It has getters and setter for every field. 
 * Additionally, it has two methods, which increase and decrease field quantity by one.
 * @author malgorzata.syska@o2.pl */
public class ProductInCart {
	
	private Product product;
	private int quantity;
	
	public ProductInCart(Product product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void increaseQuantityByOne() {
		quantity++;
	}
	
	public void decreaseQuantityByOne() {
		quantity--;
	}
}
