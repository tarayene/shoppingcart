package pl.bs.ShoppingCart;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.Before;

/** Class which test Cart class. It has three fields - (Cart) cart and two products. 
 * @author malgorzata.syska@o2.pl */
public class CartTest {
	
	private Cart cart;
	private Product productOne;
	private Product productTwo;
	
	@Before
	public void setup() {
		cart = new Cart();
		productOne = new Product("mleko","1 litr, 2%",new BigDecimal(2.0));
		productTwo = new Product("kubek","z uchem",new BigDecimal(4.2));
	}
	
	@Test
	public void willProductBeAddedWhenThereIsNoProductInCart() {
		//when
		cart.addToCart(productOne);
		
		//then
		assertEquals(cart.listOfProducts.size(), 1);
		assertEquals(cart.listOfProducts.get(0).getQuantity(), 1);
		assertEquals(cart.listOfProducts.get(0).getProduct().getName(), "mleko");
	}
	
	@Test
	public void willProductBeAddedWhenThereIsSuitableProductInCart() {
		//given
		cart.addToCart(productOne);
		
		//when
		cart.addToCart(productOne);
		
		//then
		assertEquals(cart.listOfProducts.size(), 1);
		assertEquals(cart.listOfProducts.get(0).getQuantity(), 2);
		assertEquals(cart.listOfProducts.get(0).getProduct().getName(), "mleko");
	}
	
	@Test
	public void willProductBeAddedWhenThereIsNoSuitableProductInCart() {
		//given
		cart.addToCart(productOne);
		
		//when
		cart.addToCart(productTwo);
		
		//then
		assertEquals(cart.listOfProducts.size(), 2);
		assertEquals(cart.listOfProducts.get(0).getQuantity(), 1);
		assertEquals(cart.listOfProducts.get(1).getQuantity(), 1);
		assertEquals(cart.listOfProducts.get(0).getProduct().getName(), "mleko");
		assertEquals(cart.listOfProducts.get(1).getProduct().getName(), "kubek");
	}
	
	@Test
	public void willProductBeRemovedWhenItIsInCart() {
		//given
		cart.addToCart(productOne);
		
		//when
		cart.removeFromCart(productOne);
		
		//then
		assertEquals(cart.listOfProducts.size(), 0);
	}
	
	@Test
	public void willProductBeRemovedWhenItIsNotInCart() {
		//given
		cart.addToCart(productTwo);
		
		//when
		cart.removeFromCart(productOne);
		
		//then
		assertEquals(cart.listOfProducts.size(), 1);
	}
	
	@Test
	public void testPriceWhenThereIsOneProductInCart() {
		//given
		cart.addToCart(productOne);
		
		//then
		assertEquals(cart.countPrice(),new BigDecimal(2.0).setScale(2, BigDecimal.ROUND_HALF_UP));
	}
	
	@Test
	public void testPriceWhenThereAreTwoDifferentProductsInCart() {
		///given
		cart.addToCart(productOne);
		cart.addToCart(productTwo);
		
		//then
		assertEquals(cart.countPrice(),new BigDecimal(6.20).setScale(2, BigDecimal.ROUND_HALF_UP));
	}
	
	@Test
	public void testPriceWhenThereAreTwoSameProductsInCart() {
		//given
		cart.addToCart(productOne);
		cart.addToCart(productOne);
		
		//then
		assertEquals(cart.countPrice(),new BigDecimal(4.0).setScale(2, BigDecimal.ROUND_HALF_UP));
	}
	
	@Test
	public void testPriceWhenThereAreManyDifferentProductsInCart() {
		//given
		cart.addToCart(productOne);
		cart.addToCart(productOne);
		cart.addToCart(productTwo);
		cart.addToCart(productTwo);
		cart.addToCart(productTwo);
		
		//then
		assertEquals(cart.countPrice(),new BigDecimal(16.6).setScale(2, BigDecimal.ROUND_HALF_UP));
	}
	
	@Test
	public void testTotalQuantityOfProductsWhenThereIsOneProductInCart() {
		//given
		cart.addToCart(productOne);
		
		//then
		assertEquals(cart.countProducts(), 1);
	}
	
	@Test
	public void testTotalQuantityOfProductsWhenThereAreManyProductsInCart() {
		//given
		cart.addToCart(productOne);
		cart.addToCart(productOne);
		cart.addToCart(productTwo);
		
		//then
		assertEquals(cart.countProducts(), 3);
		assertEquals(cart.listOfProducts.size(), 2);
	}
	
}
