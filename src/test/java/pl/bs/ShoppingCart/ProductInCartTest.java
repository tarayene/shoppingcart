package pl.bs.ShoppingCart;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/** Class which tests ProductInCart class. It has one field - productInCart. 
 * @author malgorzata.syska@o2.pl */
public class ProductInCartTest {

	private ProductInCart productInCart;

	@Before
	public void setup() {
		productInCart = new ProductInCart(null, 1);
	}

	@Test
	public void shouldQuantityBeIncreasedByOneWhenIncreasingQuantity() {
		// when
		productInCart.increaseQuantityByOne();

		// then
		assertEquals(productInCart.getQuantity(), 2);
	}

	@Test
	public void shouldQuantityBeDecreasedByOneWhenDesceasingQuantity() {
		// when
		productInCart.decreaseQuantityByOne();
		
		// then
		assertEquals(productInCart.getQuantity(), 0);
	}

}
